<?php

namespace AppBundle\Service;

use AppBundle\Model\Contact as Model;

class Contact
{
    /** @var \Swift_Mailer */
    private $mailer;

    /**
     * @param \Swift_Mailer $mailer
     */
    public function __construct(\Swift_Mailer $mailer)
    {
        $this->mailer = $mailer;
    }

    /**
     * @param Model $contact
     */
    public function sendEmail(Model $contact)
    {
        $message = \Swift_Message::newInstance()
            ->setTo('nicolas.dewez@sensiolabs.com')
            ->setFrom($contact->getEmail())
            ->setSubject($contact->getSubject())
            ->setBody($contact->getMessage())
        ;

        $this->mailer->send($message);
    }
}

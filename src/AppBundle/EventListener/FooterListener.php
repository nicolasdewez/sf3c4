<?php

namespace AppBundle\EventListener;

use Symfony\Component\HttpKernel\Event\FilterResponseEvent;

class FooterListener
{
    /**
     * @param FilterResponseEvent $event
     */
    public function addFooter(FilterResponseEvent $event)
    {
        if (!$event->isMasterRequest()) {
            return;
        }

        if ($event->getRequest()->isXmlHttpRequest()) {
            return;
        }

        $response = $event->getResponse();
        $response->setContent($response->getContent().'<br>Ceci est un texte ajouté par un listener');
    }
}
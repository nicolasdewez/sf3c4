<?php

namespace AppBundle\EventListener;

use Symfony\Component\HttpKernel\Event\GetResponseEvent;

class VisitorListener
{
    /** @var string */
    private $pathVisitor;
    /**
     * @param string $pathVisitor
     */
    public function __construct(string $pathVisitor)
    {
        $this->pathVisitor = $pathVisitor;
    }

    /**
     * @param GetResponseEvent $event
     */
    public function addVisitor(GetResponseEvent $event)
    {
        if (!$event->isMasterRequest()) {
            return;
        }

        $file = fopen($this->pathVisitor, 'a');
        fputs($file, $event->getRequest()->getPathInfo()."\n");
        fclose($file);
    }
}

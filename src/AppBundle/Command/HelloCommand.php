<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class HelloCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('app:hello')
            ->setDescription('Say Hello')
            ->addArgument('name', InputArgument::REQUIRED, 'To Who?')
            ->addOption('cry', 'c', InputOption::VALUE_NONE, 'CRY!')
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $name = $input->getArgument('name');
        $cry = $input->getOption('cry');

        $string = sprintf('Hello %s', $name);
        if ($cry) {
            $string = sprintf('%s!', strtoupper($string));
        }

        $output->writeln($string);
    }
}












<?php

namespace AppBundle\Game;

use AppBundle\Game\Exception\NoContextsFoundException;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class GameContext implements GameContextInterface
{
    private $session;

    public function __construct(SessionInterface $session)
    {
        $this->session  = $session;
    }

    public function reset()
    {
        $this->session->set('hangman', array());
    }

    /**
     * {@inheritdoc}
     */
    public function newGame(string $word): Game
    {
        return new Game($word);
    }

    /**
     * {@inheritdoc}
     */
    public function loadGame(): Game
    {
        $data = $this->session->get('hangman');

        if (!$data) {
            throw new NoContextsFoundException('No data found');
        }

        return new Game(
            $data['word'],
            $data['attempts'],
            $data['tried_letters'],
            $data['found_letters']
        );
    }

    /**
     * {@inheritdoc}
     */
    public function save(Game $game)
    {
        $this->session->set('hangman', $game->getContext());
    }
}

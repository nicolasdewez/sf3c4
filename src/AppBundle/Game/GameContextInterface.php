<?php

namespace AppBundle\Game;

interface GameContextInterface
{
    /**
     * Resets the current game context
     *
     * @return void
     */
    public function reset();

    /**
     * Creates a new Game instance.
     *
     * @param string $word The word to be guessed
     * @return Game
     */
    public function newGame(string $word): Game;

    /**
     * Loads an existing game.
     *
     * @return Game
     */
    public function loadGame(): Game;

    /**
     * Saves the provided game.
     *
     * @param Game $game The game to save
     * @return void
     */
    public function save(Game $game);
}
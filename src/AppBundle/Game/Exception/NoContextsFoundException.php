<?php

namespace AppBundle\Game\Exception;

class NoContextsFoundException extends \Exception
{
}

<?php

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use AppBundle\Form\LoginType;
use AppBundle\Form\RegisterType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class SecurityController extends Controller
{
    /**
     * @return Response
     *
     * @Route("/login", name="app_login", methods={"GET"})
     */
    public function indexAction(): Response
    {
        $form = $this->createForm(LoginType::class);
        $helper = $this->get('security.authentication_utils');

        return $this->render('security/index.html.twig', ['form' => $form->createView(), 'error' => $helper->getLastAuthenticationError()]);
    }

    /**
     * @param Request $request
     *
     * @return Response
     *
     * @Route("/register", name="app_register", methods={"GET", "POST"})
     */
    public function registerAction(Request $request): Response
    {
        $user = new User();
        $form = $this->createForm(RegisterType::class, $user);
        $form->add('submit', SubmitType::class, ['label' => 'form.submit']);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $encoder = $this->get('security.password_encoder');
            $user->setPassword($encoder->encodePassword($user, $user->getPassword()));

            $manager = $this->get('doctrine')->getManager();
            $manager->persist($user);
            $manager->flush();

            $this->addFlash('info', 'L\'utilisateur a été créé');

            return $this->redirectToRoute('app_login');
        }

        return $this->render('security/register.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/game/fr/login-check", name="app_login_check", methods={"POST"})
     */
    public function loginCheckAction()
    {
        die('Cette action n\'est pas exécutée');
    }

    /**
     * @Route("/game/fr/logout", name="app_logout", methods={"GET"})
     */
    public function logoutAction()
    {
        die('Cette action n\'est pas exécutée');
    }
}

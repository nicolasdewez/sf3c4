<?php

namespace AppBundle\Controller;

use AppBundle\Game\GameContext;
use AppBundle\Game\GameRunner;
use AppBundle\Game\Loader\TextFileLoader;
use AppBundle\Game\Loader\XmlFileLoader;
use AppBundle\Game\WordList;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route("/game/{_locale}", defaults={"_locale": "fr"}, requirements={"_locale": "^en|fr$"})
 */
class GameController extends Controller
{
    /**
     * @return Response
     *
     * @Route("", name="app_game", methods={"GET"})
     */
    public function gameAction(): Response
    {
        return $this->render('game/game.html.twig', ['game' => $this->get('app.game_runner')->loadGame($this->getParameter('word_length'))]);
    }

    /**
     * @param string $letter
     *
     * @return Response
     *
     * @Route("/letter/{letter}", name="app_letter", methods={"GET"}, defaults={"letter": "A"})
     */
    public function tryLetterAction(string $letter): Response
    {
//        dump($request->attributes->get('letter'));
//        dump($request->get('letter'));
//        dump($request->query->get('param'));

        $game = $this->get('app.game_runner')->playLetter($letter);
        if (!$game->isOver()) {
            return $this->redirectToRoute('app_game');
        }

        return $this->redirectToRoute($game->isWon() ? 'app_game_won' : 'app_game_failed');
    }

    /**
     * @param Request $request
     *
     * @return Response
     *
     * @Route("/word", name="app_word", methods={"POST"})
     */
    public function tryWordAction(Request $request): Response
    {
        $game = $this->get('app.game_runner')->playWord($request->request->get('word'));

        return $this->redirectToRoute($game->isWon() ? 'app_game_won' : 'app_game_failed');
    }

    /**
     * @return Response
     *
     * @Route("/won", name="app_game_won", methods={"GET"})
     */
    public function wonAction(): Response
    {
        $gameRunner = $this->get('app.game_runner');

        $word = $gameRunner->loadGame($this->getParameter('word_length'))->getWord();
        $gameRunner->resetGameOnSuccess();

        return $this->render('game/won.html.twig', ['word' => $word]);
    }

    /**
     * @return Response
     *
     * @Route("/failed", name="app_game_failed", methods={"GET"})
     */
    public function failedAction(): Response
    {
        $gameRunner = $this->get('app.game_runner');

        $word = $gameRunner->loadGame($this->getParameter('word_length'))->getWord();
        $gameRunner->resetGameOnFailure();

        return $this->render('game/failed.html.twig', ['word' => $word]);
    }

    /**
     * @return Response
     *
     * @Route("/reset", name="app_game_reset", methods={"GET"})
     */
    public function resetAction(): Response
    {
        $this->get('app.game_runner')->resetGame();

        return $this->redirectToRoute('app_game');
    }

    public function lastPlayersAction()
    {
        return $this->render('game/lastPlayers.html.twig');
    }

    /**
     * @return Response
     *
     * @Cache(smaxage=20)
     */
    public function lastGamesAction()
    {
        return $this->render('game/lastGames.html.twig');
    }

    /**
     * @return GameRunner
     */
    private function getGameRunner(): GameRunner
    {
        $wordList = new WordList();

        $wordList->addLoader('txt', new TextFileLoader());
        $wordList->addLoader('xml', new XmlFileLoader());

        $wordList->loadDictionaries([
            $this->getParameter('kernel.root_dir').'/Resources/data/words.txt',
            $this->getParameter('kernel.root_dir').'/Resources/data/words.xml',
        ]);

        $gameContext = new GameContext($this->get('session'));
        // Ou Request en paramètre et $request->getSession();

        return new GameRunner($gameContext, $wordList);
    }
}











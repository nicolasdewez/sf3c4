<?php

namespace AppBundle\Controller;

use AppBundle\Form\ContactType;
use AppBundle\Model\Contact;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('default/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.root_dir').'/..').DIRECTORY_SEPARATOR,
        ]);
    }

    /**
     * @Route("/hello-world", name="app_hello_world")
     */
    public function helloWorldAction()
    {
        return new Response('Hello world !');
    }

    /**
     * @Route(
     *     "/hello/{name}",
     *     name="app_hello",
     *     methods={"GET"},
     *     requirements={"name": "^[A-Za-z-]+$"}
     * )
     */
    public function helloAction($name)
    {
        return new Response(sprintf('Hello %s !', $name));
    }

    /**
     * @param Request $request
     *
     * @return Response
     *
     * @Route("/contact", name="app_contact", methods={"GET", "POST"})
     */
    public function contactAction(Request $request): Response
    {
        $contact = new Contact();
        $form = $this->createForm(ContactType::class, $contact);
        $form->add('submit', SubmitType::class, ['label' => 'form.submit']);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->get('app.contact')->sendEmail($contact);
            $this->addFlash('info', 'Mail envoyé !');
            $request->getSession()->getFlashBag()->add('info', 'Mail envoyé 2 !');
            $this->get('session')->getFlashBag()->add('info', 'Mail envoyé 3 !');

            return $this->redirectToRoute('app_game');
        }

        return $this->render('default/contact.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @param Contact $contact
     */
    private function sendEmail(Contact $contact)
    {
        $message = \Swift_Message::newInstance()
            ->setTo('nicolas.dewez@sensiolabs.com')
            ->setFrom($contact->getEmail())
            ->setSubject($contact->getSubject())
            ->setBody($contact->getMessage())
        ;

        $this->get('mailer')->send($message);
    }
}














<?php

namespace tests\AppBundle\Game;

use AppBundle\Game\Loader\LoaderInterface;
use AppBundle\Game\WordList;

class WordListTest extends \PHPUnit_Framework_TestCase
{
    public function testLoadDictionaries()
    {
        $loader = $this->createMock(LoaderInterface::class);
        $loader
            ->expects($this->once())
            ->method('load')
            ->with('file.txt')
            ->willReturn(['php', 'java', 'ruby'])
        ;

        $wordList = new WordList();
        $wordList->addLoader('txt', $loader);

        $wordList->loadDictionaries(['file.txt']);
        $this->assertSame('php', $wordList->getRandomWord(3));
    }
}

<?php

namespace tests\AppBundle\Game;

use AppBundle\Game\Game;

class GameTest extends \PHPUnit_Framework_TestCase
{
    public function testTryWordFailed()
    {
        $game = new Game('php');

        $this->assertFalse($game->tryWord('Symfony'));
        $this->assertSame(Game::MAX_ATTEMPTS, $game->getAttempts());
    }

    public function testTryWordCorrect()
    {
        $game = new Game('php');

        $this->assertTrue($game->tryWord('php'));
        $this->assertSame(['p', 'h'], $game->getFoundLetters());

        // Or
        $foundLetters = $game->getFoundLetters();
        $this->assertContains('p', $foundLetters);
        $this->assertContains('h', $foundLetters);
        $this->assertCount(2, $foundLetters);
    }

    /**
     * @expectedException \InvalidArgumentException
     */
    public function testTryLetterInvalid()
    {
        //$this->expectException('\InvalidArgumentException');
        //$this->expectException(\InvalidArgumentException::class);

        $game = new Game('php');
        $game->tryLetter('2');
    }

    public function testTryLetterOk()
    {
        $game = new Game('php');

        $this->assertTrue($game->tryLetter('p'));
        $this->assertSame(0, $game->getAttempts());
        $this->assertSame(['p'], $game->getFoundLetters());
        $this->assertSame(['p'], $game->getTriedLetters());
    }

    public function testTryLetterAlreadyUsed()
    {
        $game = new Game('php');
        $game->tryLetter('p');

        $this->assertFalse($game->tryLetter('p'));
        $this->assertSame(1, $game->getAttempts());
        $this->assertSame(['p'], $game->getFoundLetters());
        $this->assertSame(['p'], $game->getTriedLetters());
    }

    public function testTryLetterKo()
    {
        $game = new Game('php');

        $this->assertFalse($game->tryLetter('x'));
        $this->assertSame(1, $game->getAttempts());
        $this->assertSame([], $game->getFoundLetters());
        $this->assertSame(['x'], $game->getTriedLetters());
    }
}














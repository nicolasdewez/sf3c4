<?php

namespace Tests\AppBundle\Controller;

use AppBundle\Game\Game;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class GameControllerTest extends WebTestCase
{
    public function testRedirectionForTryLetter()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/game');
//        $letter = $crawler->selectLink('P')->link();
        $letter = $crawler->filter('.letter a:contains("P")')->eq(0)->link();
        $client->click($letter);

        $this->assertTrue($client->getResponse()->isRedirection());
    }

    public function testWin()
    {
        $client = static::createClient();
        $client->followRedirects(true);

        $crawler = $client->request('GET', '/game');

        $letter = $crawler->filter('.letter a:contains("P")')->eq(0)->link();
        $crawler = $client->click($letter);
        $letter = $crawler->filter('.letter a:contains("H")')->eq(0)->link();
        $client->click($letter);

        //$client->getContainer()->get('router')->generate('app_game_won');

        $this->assertSame('/game/fr/won', $client->getRequest()->getPathInfo());
    }

    public function testFail()
    {
        $client = static::createClient();
        $client->followRedirects(true);

        $crawler = $client->request('GET', '/game');

        for ($i=1; $i<=Game::MAX_ATTEMPTS; $i++) {
            $letter = $crawler->filter('.letter a:contains("Z")')->eq(0)->link();
            $crawler = $client->click($letter);
        }

        $this->assertSame('/game/fr/failed', $client->getRequest()->getPathInfo());
    }
}
